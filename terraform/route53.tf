# dns recordds for all the nodes
## public zone
resource "aws_route53_record" "master-nodes" {
  count = 3
  zone_id = "Z2XGPWV1M639TV" # openshift-comsysto.com
  name    = "openshift-master${count.index +1}"
  type    = "A"
  ttl     = "300"
  records = ["${element(aws_instance.master-node.*.public_ip, count.index)}"]
}

resource "aws_route53_record" "nodes" {
  count = 2
  zone_id = "Z2XGPWV1M639TV" # openshift-comsysto.com
  name    = "openshift-node${count.index +1}"
  type    = "A"
  ttl     = "300"
  records = ["${element(aws_instance.node.*.public_ip, count.index)}"]
}

resource "aws_route53_record" "infra-nodes" {
  count = 2
  zone_id = "Z2XGPWV1M639TV" # openshift-comsysto.com
  name    = "openshift-infranode${count.index +1}"
  type    = "A"
  ttl     = "300"
  records = ["${element(aws_instance.infra-node.*.public_ip, count.index)}"]
}

resource "aws_route53_record" "haproxy" {
  zone_id = "Z2XGPWV1M639TV" # openshift-comsysto.com
  name    = "openshift-haproxy"
  type    = "A"
  ttl     = "300"
  records = ["${element(aws_instance.haproxy.*.public_ip, count.index)}"]
}
resource "aws_route53_record" "clusterdns" {
  zone_id = "Z2XGPWV1M639TV" # openshift-comsysto.com
  name    = "openshift-cluster"
  type    = "A"
  ttl     = "300"
  records = ["${element(aws_instance.master-node.*.public_ip, count.index)}"]
}

## private zone

resource "aws_route53_zone" "priv" {
  name   = "comsysto.com"
  vpc_id = "${aws_vpc.openshift.id}"
}

resource "aws_route53_record" "master-nodes-priv" {
  count = 3
  zone_id = "${aws_route53_zone.priv.id}"
  name    = "openshift-master${count.index +1}"
  type    = "A"
  ttl     = "300"
  records = ["${element(aws_instance.master-node.*.private_ip, count.index)}"]
}

resource "aws_route53_record" "nodes-priv" {
  count = 2
  zone_id = "${aws_route53_zone.priv.id}"
  name    = "openshift-node${count.index +1}"
  type    = "A"
  ttl     = "300"
  records = ["${element(aws_instance.node.*.private_ip, count.index)}"]
}

resource "aws_route53_record" "infra-nodes-priv" {
  count = 2
  zone_id = "${aws_route53_zone.priv.id}"
  name    = "openshift-infranode${count.index +1}"
  type    = "A"
  ttl     = "300"
  records = ["${element(aws_instance.infra-node.*.private_ip, count.index)}"]
}

resource "aws_route53_record" "haproxy-priv" {
  zone_id = "${aws_route53_zone.priv.id}"
  name    = "openshift-haproxy"
  type    = "A"
  ttl     = "300"
  records = ["${element(aws_instance.haproxy.*.private_ip, count.index)}"]
}
resource "aws_route53_record" "clusterdns-priv" {
  zone_id = "${aws_route53_zone.priv.id}"
  name    = "openshift-cluster"
  type    = "A"
  ttl     = "300"
  records = ["${element(aws_instance.master-node.*.private_ip, count.index)}"]
}
