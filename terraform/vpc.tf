# bring up the network infrastructure (vpc, subnets, igw, nat gateway, routes, sec. group rules)

resource "aws_vpc" "openshift" {
  cidr_block       = "10.0.0.0/16"
  enable_dns_hostnames = true
  tags {
    Name = "openshift"
  }
}

resource "aws_subnet" "openshift-pub" {
  vpc_id                  = "${aws_vpc.openshift.id}"
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  tags {
    Name = "openshift-pub"
    "kubernetes.io/cluster/name" = "comsysto_os_cluster"
  }
}

resource "aws_subnet" "openshift-private" {
  vpc_id     = "${aws_vpc.openshift.id}"
  cidr_block = "10.0.2.0/24"

  tags {
    Name = "openshift-private"
  }
}

resource "aws_internet_gateway" "openshift-igw" {
  vpc_id = "${aws_vpc.openshift.id}"

  tags {
    Name = "main"
  }
}

resource "aws_eip" "nat-gateway-eip" {
  vpc  = true
}

resource "aws_nat_gateway" "gw" {
  allocation_id = "${aws_eip.nat-gateway-eip.id}"
  subnet_id     = "${aws_subnet.openshift-pub.id}"
}

resource "aws_route" "igw_route" {
  route_table_id            = "${aws_vpc.openshift.default_route_table_id}"
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id                = "${aws_internet_gateway.openshift-igw.id}"
 
}

resource "aws_security_group_rule" "ssh" {
  type            = "ingress"
  from_port       = 22
  to_port         = 22
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  
  security_group_id = "${aws_vpc.openshift.default_security_group_id}"
}


resource "aws_security_group_rule" "allow-all" {
  type            = "ingress"
  from_port       = 0
  to_port         = 65535
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  
  security_group_id = "${aws_vpc.openshift.default_security_group_id}"
}
