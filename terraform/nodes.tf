resource "aws_instance" "master-node" {
  count         = 3
  ami           = "ami-ee6a718a"
  instance_type = "t2.xlarge"
  key_name      = "delboy"
  tags {
     Name = "openshift-master${count.index +1}"
     "kubernetes.io/cluster/name" = "comsysto_os_cluster"
  }

  root_block_device {
     volume_type = "gp2"
     volume_size = 50  
  }

  subnet_id="${aws_subnet.openshift-pub.id}"
 
}

resource "aws_instance" "node" {
  count = 2
  ami = "ami-ee6a718a"
  instance_type = "t2.large"
  key_name      = "delboy"
  tags {
     Name = "openshift-node${count.index +1}"
     "kubernetes.io/cluster/name" = "comsysto_os_cluster"
  }

  root_block_device {
     volume_type = "gp2"
     volume_size = 20
  }

  subnet_id="${aws_subnet.openshift-pub.id}"

}

resource "aws_instance" "infra-node" {
  count = 2
  ami = "ami-ee6a718a"
  instance_type = "t2.large"
  key_name      = "delboy"
  tags {
    Name = "openshift-infra-node${count.index +1}"
    "kubernetes.io/cluster/name" = "comsysto_os_cluster"
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = 20
  }

  subnet_id="${aws_subnet.openshift-pub.id}"

}

resource "aws_instance" "haproxy" {
  count = 1
  ami = "ami-ee6a718a"
  instance_type = "t2.large"
  key_name      = "delboy"
  tags {
    Name = "openshift-haproxy"
    "kubernetes.io/cluster/name" = "comsysto_os_cluster"
  }
   
  root_block_device {
    volume_type = "gp2"
    volume_size = 20
  }

  subnet_id="${aws_subnet.openshift-pub.id}"

}

resource "aws_instance" "nfs-server" {
  ami           = "ami-ee6a718a"
  instance_type = "t2.large"
  key_name      = "delboy"
  tags {
     Name = "openshift-nfs"
     "kubernetes.io/cluster/name" = "comsysto_os_cluster"
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = 20
  }

  ebs_block_device {
    
    device_name = "/dev/sdb"
    volume_type = "gp2"
    volume_size = 100
  }

   subnet_id="${aws_subnet.openshift-pub.id}"

}




