# Create an OSEv3 group that contains the master, nodes, etcd, and lb groups.
# The lb group lets Ansible configure HAProxy as the load balancing solution.
# Comment lb out if your load balancer is pre-configured.
[OSEv3:children]
masters
nodes
etcd
lb

# Set variables common for all OSEv3 hosts
[OSEv3:vars]
ansible_ssh_user=centos
ansible_become=yes
openshift_deployment_type=origin
skip_sanity_checks=true

# Uncomment the following to enable htpasswd authentication; defaults to
# DenyAllPasswordIdentityProvider.
#openshift_master_identity_providers=[{'name': 'htpasswd_auth', 'login': 'true', 'challenge': 'true', 'kind': 'HTPasswdPasswordIdentityProvider'}]

# Native high availability cluster method with optional load balancer.
# If no lb group is defined installer assumes that a load balancer has
# been preconfigured. For installation the value of
# openshift_master_cluster_hostname must resolve to the load balancer
# or to one or all of the masters defined in the inventory if no load
# balancer is present.
openshift_master_cluster_method=native
openshift_master_cluster_hostname=openshift-cluster.comsysto.com
openshift_master_cluster_public_hostname=openshift-cluster.comsysto.com

# Cloud Provider Configuration
#
# Note: You may make use of environment variables rather than store
# sensitive configuration within the ansible inventory.
# For example:
#openshift_cloudprovider_aws_access_key="{{ lookup('env','AWS_ACCESS_KEY_ID') }}"
#openshift_cloudprovider_aws_secret_key="{{ lookup('env','AWS_SECRET_ACCESS_KEY') }}"
#
openshift_clusterid=comsysto_os_cluster
#
# AWS (Using API Credentials)
openshift_cloudprovider_kind=aws
openshift_cloudprovider_aws_access_key=
openshift_cloudprovider_aws_secret_key=
#
# AWS (Using IAM Profiles)
#openshift_cloudprovider_kind=aws
# Note: IAM roles must exist before launching the instances.

#openshift_node_groups=[ "masters", "etcd", "lb", "nodes", "infra" ]

masters="{ 'name': 'masters', 'labels': ['node-role.kubernetes.io/master=true'] }"
etcd="{ 'name': 'etcd', 'labels': ['node-role.kubernetes.io/etcd=true'] }"
lb="{ 'name': 'lb', 'labels': ['node-role.kubernetes.io/lb=true'] }"
nodes="{ 'name': 'nodes', 'labels': ['node-role.kubernetes.io/node=true'] }"
infra="{ 'name': 'infra', 'labels': ['node-role.kubernetes.io/infra=true'] }"


openshift_node_groups=[ "masters", "etcd", "lb", "nodes", "infra" ]

# host group for masters
[masters]
#openshift-master1.comsysto.com openshift_hostname=openshift-master1.comsysto.com openshift_public_hostname=openshift-master1.comsysto.com
#openshift-master2.comsysto.com openshift_hostname=openshift-master2.comsysto.com openshift_public_hostname=openshift-master2.comsysto.com
#openshift-master3.comsysto.com openshift_hostname=openshift-master3.comsysto.com openshift_public_hostname=openshift-master3.comsysto.com
ip-10-0-1-6.eu-west-2.compute.internal
ip-10-0-1-188.eu-west-2.compute.internal
ip-10-0-1-59.eu-west-2.compute.internal

[masters:vars]
openshift_node_group_name="masters"

# host group for etcd
[etcd]
#openshift-master1.comsysto.com openshift_hostname=openshift-master1.comsysto.com openshift_public_hostname=openshift-master1.comsysto.com
#openshift-master2.comsysto.com openshift_hostname=openshift-master2.comsysto.com openshift_public_hostname=openshift-master2.comsysto.com
#openshift-master3.comsysto.com openshift_hostname=openshift-master3.comsysto.com openshift_public_hostname=openshift-master3.comsysto.com

ip-10-0-1-6.eu-west-2.compute.internal
ip-10-0-1-188.eu-west-2.compute.internal
ip-10-0-1-59.eu-west-2.compute.internal


[etcd:vars]
openshift_node_group_name="etcd"

[lb]
openshift-haproxy.comsysto.com openshift_hostname=openshift-haproxy.comsysto.com

[lb:vars]
openshift_node_group_name="lb"

# host group for nodes, includes region info
[nodes]
#openshift-master[1:3].comsysto.com #openshift_hostname=openshift-master[1:3].comsysto.com openshift_public_hostname=openshift-master[1:3].comsysto.com
#openshift-node1.comsysto.com openshift_node_labels="{'region': 'primary', 'zone': 'east'}" openshift_hostname=openshift-node1.comsysto.com openshift_public_hostname=openshift-node1.comsysto.com
#openshift-node2.comsysto.com openshift_node_labels="{'region': 'primary', 'zone': 'west'}" openshift_hostname=openshift-node2.comsysto.com openshift_public_hostname=openshift-node2.comsysto.com
#openshift-infranode1.comsysto.com openshift_node_labels="{'region': 'infra', 'zone': 'default'}" openshift_hostname=openshift-infranode1.comsysto.com openshift_public_hostname=openshift-infranode1.comsysto.com
#openshift-infranode2.comsysto.com openshift_node_labels="{'region': 'infra', 'zone': 'default'}" openshift_hostname=openshift-infranode2.comsysto.com openshift_public_hostname=openshift-infranode2.comsysto.com

ip-10-0-1-6.eu-west-2.compute.internal
ip-10-0-1-188.eu-west-2.compute.internal
ip-10-0-1-59.eu-west-2.compute.internal

ip-10-0-1-96.eu-west-2.compute.internal  openshift_node_labels="{'region': 'primary', 'zone': 'east'}"
ip-10-0-1-248.eu-west-2.compute.internal openshift_node_labels="{'region': 'primary', 'zone': 'west'}"

ip-10-0-1-75.eu-west-2.compute.internal openshift_node_labels="{'region': 'infra', 'zone': 'default'}"
ip-10-0-1-149.eu-west-2.compute.internal openshift_node_labels="{'region': 'infra', 'zone': 'default'}"



[nodes:vars]
openshift_node_group_name="nodes"


#[infra]
#openshift-infranode1.comsysto.com openshift_node_labels="{'region': 'infra', 'zone': 'default'}"
#openshift-infranode2.comsysto.com openshift_node_labels="{'region': 'infra', 'zone': 'default'}"

#[infra:vars]
#openshift_node_group_name="infra"


openshift_node_groups=[ "masters", "etcd", "lb", "nodes" ]
